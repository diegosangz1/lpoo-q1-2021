from models.salario import Salary
from models.comision import Commission
import os


if __name__ == '__main__':
    while True:
        print("\n -----> Menu Principal Parcial 1!! <----- \n")
        print("Seleccione la opcion del Menu: ")
        print("1. Programa para Calcular Salario Neto.")
        print("2. Programa para Calcular Comision de Venta.")
        print("3. Exit")
        select = input("El numero de su seleccion es: ")
        if select == "1":
            salario = Salary()
            salario.reg_sal()
            salario.calc_sal()
            os.system("pause")
        elif select == "2":
            comision = Commission()
            comision.reg_sales()
            comision.calc_comm()
            os.system("pause")
        elif select == "3":
            break