class Cellphone:
    brand = ""
    model = ""
    price = 0.0

    def request_info(self):
        print("Device Information: ")
        self.brand = input("Enter Device Brand >> ")
        self.model = input("Enter Device Model >> ")
        self.price = input("Enter Device Price >> ")

    def sale(self):
        print("Your Cellphone is from ", self.brand)
        print("The Model is ", self.model)
        print("Sub-Total = ", self.price)
        tax = float(self.price) * 0.07
        total = float(self.price) + tax
        print("Tax = ", tax)
        print("Total = ", total)


mobile = Cellphone()

mobile.request_info()
mobile.sale()
