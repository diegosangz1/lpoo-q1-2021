class Associate:
    name = ""
    l_name = ""
    year_hired = 0
    status = ""

    def request_info(self):
        print("Enter the associate's information:  ")
        self.name = input("Name >> ")
        self.l_name = input("Last Name >> ")
        self.year_hired = input("Year hired >> ")
        self.status = input("Status >> ")

    def show_info(self):
        print("The associate named ", self.name, self.l_name)
        if self.status == "Hired" or self.status == "hired":
            print("Its currently employed")
            time = 2021 - int(self.year_hired)
            print("And has been working for the company for ", time, "years")
        else:
            print("Its no longer working for the company")


cw = Associate()

cw.request_info()
cw.show_info()
