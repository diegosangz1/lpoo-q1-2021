class Car:
    brand = ""
    model = ""
    make = 0

    def request_info(self):
        print("Enter your car details: ")
        self.brand = input("Enter your car's brand >> ")
        self.model = input("Enter your car's model >> ")
        self.make = input("Enter your car's make >> ")

    def car_age(self):
        print("Your car's brand is ", self.brand)
        print("It's model is ", self.model)
        print("Was made in ", self.make)
        age = 2021 - int(self.make)
        print("And it's ", age, "years old")


auto = Car()

auto.request_info()
auto.car_age()