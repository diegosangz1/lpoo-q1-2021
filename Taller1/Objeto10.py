class Painting:
    technique = ""
    year_painted = 0
    author = ""
    price = 0.0

    def request_info(self):
        print("Painting information: ")
        self.technique = input("Enter the technique used on the painting >> ")
        self.year_painted = input("Enter the year it was painted on >> ")
        self.author = input("Enter the artist name >> ")
        self.price = input("Enter the  price >> ")

    def sale(self):
        print("This is an ", self.technique, "painting")
        print("Done by ", self.author)
        age = 2021 - int(self.year_painted)
        print("Painted ", age, "years ago")
        print("Sub-Total is ", self.price)
        tax = float(self.price) * 0.07
        total = float(self.price) + tax
        print("Tax is ", tax)
        print("Your Total is ", total)


art = Painting()

art.request_info()
art.sale()