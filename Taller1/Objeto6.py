class Plushy:
    material = ""
    brand = ""
    color = ""
    price = 0.0

    def request_info(self):
        print("Plushy toy information: ")
        self.material = input("Enter the plushy size >> ")
        self.brand = input("Enter the plushy brand >> ")
        self.color = input("Enter the plushy color >> ")
        self.price = input("Enter the plushy price >> ")

    def sale(self):
        print("The plushy toy is from ", self.brand)
        print("Its made of ", self.material)
        print("Its predominant color is ", self.color)
        print("Sub-Total is ", self.price)
        tax = float(self.price) * 0.07
        total = float(self.price) + tax
        print("Tax is ", tax)
        print("Your Total is ", total)


toy = Plushy()

toy.request_info()
toy.sale()