class Television:
    size = 0
    brand = ""
    price = 0.0
    discount = 0

    def request_info(self):
        print("Enter the television details: ")
        self.size = input("Enter its size >> ")
        self.brand = input("Enter its brand >> ")
        self.price = input("Enter its prize >>")
        self.discount = input("Enter discount to be applied % >> ")

    def sale(self):
        print("The television its ", self.size, "inches ")
        print("Was made by  ", self.brand)
        print("Its price is ", self.price)
        dcount = float(self.price) * (int(self.discount)/100)
        total = float(self.price) - dcount
        print("Your Total with discount is ", total)


tv = Television()

tv.request_info()
tv.sale()