class Estudiante:
    nombre = ""
    apellido = ""
    carrera = ""

    def introducir_info(self):
        print("Introduzca informacion del estudiante: ")
        self.nombre = input("Nombre >> ")
        self.apellido = input("Apellido >> ")
        self.carrera = input("Carrera >> ")

    def mostrar_info(self):
        print("Nombre del Estudiante: ", self.nombre)
        print("Apellido del Estudiante: ", self.apellido)
        print("Carrera del Estudiante: ", self.carrera)


ingreso = Estudiante()

ingreso.introducir_info()
ingreso.mostrar_info()