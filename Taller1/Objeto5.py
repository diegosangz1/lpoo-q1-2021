class Sneakers:
    size = 0
    brand = ""
    color = ""
    price = 0.0

    def request_info(self):
        print("Sneakers information: ")
        self.size = input("Enter the sneakers size >> ")
        self.brand = input("Enter the sneakers brand >> ")
        self.color = input("Enter the sneakers color >> ")
        self.price = input("Enter the sneakers price >> ")

    def sale(self):
        print("The sneakers are from ", self.brand)
        print("They are ", self.color, " color")
        print("Its size is ", self.size)
        print("Sub-Total is ", self.price)
        tax = float(self.price) * 0.07
        total = float(self.price) + tax
        print("Tax is ", tax)
        print("Your Total is ", total)


shoe = Sneakers()

shoe.request_info()
shoe.sale()