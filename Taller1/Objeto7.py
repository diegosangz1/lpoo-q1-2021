class Person:
    name = ""
    l_name = ""
    birth_year = 0

    def request_info(self):
        print("Enter your car details: ")
        self.name = input("Enter your name >> ")
        self.l_name = input("Enter your last name >> ")
        self.birth_year = input("Enter birth year >> ")

    def car_age(self):
        print("Hello,  ", self.name, self.l_name)
        print("Your birth year is ", self.birth_year)
        age = 2021 - int(self.birth_year)
        print("And you'll be ", age, "years old this year")


human = Person()

human.request_info()
human.car_age()