class Sofa:
    size = ""
    color = ""
    price = 0.0
    discount = 0

    def request_info(self):
        print("Enter the sofa details: ")
        self.size = input("Enter its size >> ")
        self.color = input("Enter its color >> ")
        self.price = input("Enter its prize >>")
        self.discount = input("Enter discount to be applied % >> ")

    def sale(self):
        print("The sofa its ", self.size)
        print("Its color is ", self.color)
        print("Its price is ", self.price)
        dcount = float(self.price) * (int(self.discount)/100)
        total = float(self.price) - dcount
        print("Your Total with discount is ", total)


chair = Sofa()

chair.request_info()
chair.sale()